# Lab Work 3 | Teoria Algoritmica da Informação

## Instruções para executar a aplicação em Linux
### Requerimentos
- Python 3.7
#### Python Libraries
- Pillow
- Numpy

### Antes de começar
Verificar versão Python3 do sistema

```
* python3 -V
```

#### Caso não tenha a versão correta do Python

```
* sudo apt-get install python3.7
* sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
* sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
* sudo update-alternatives --config python3
  * select python 3.7
* python3 -V
```

#### Caso não tenha as bibliotecas

```
* pip3 install Pillow
* pip3 install Numpy
```

### Executar

* Exemplo para execução do programa (`python3 [nome_programa] [parâmetros]`):

De forma a não gerar cache do compilador python pode ser utilizado o parâmetro `-B` aquando a sua execução
```
python3 -B [nome_programa] [parâmetros]
```

  * TEST_NCD
    * `python3 TEST_NCD.py [metodo_compressao] <optional: [negative] [resize]>`
    ```
    LZMA:
    python3 TEST_NCD.py lzma

    BZIP2:
    python3 TEST_NCD.py bzip2

    ZIP_DEFLATED:
    python3 TEST_NCD.py zip_deflated

    GZIP:
    python3 TEST_NCD.py gzip
    ```
    * Parâmetros opcionais:

      * Inverter esquema de cores das imagens:
      ```
      python3 TEST_NCD.py lzma negative
      ```

      * Redimensionar dimensões das imagens:
      ```
      python3 TEST_NCD.py lzma resize
      ```

      * Inverter esquema de cores e redimensionar dimensões das imagens:
      ```
      python3 TEST_NCD.py lzma negative resize
      ```
      ou
      ```
      python3 TEST_NCD.py lzma resize negative
      ```

#### Nota:
Os scripts encontam-se no diretório `/ImgCondComp/examples`

### Comandos úteis
- Dar permissão de execucao aos programas:

```
sudo chmod 744 [nome_programa/script]
```
## Programado com recurso a
* Microsoft Visual Studio Code

## Autores
* **André Moreira** - **Hélder Paraense**

## Sobre
* Trabalho efectuado para a unidade curricular Teoria Algoritmica da Informação do mestrado de Engenharia Informática da Universidade de Aveiro. Entregue a 2020/01/29