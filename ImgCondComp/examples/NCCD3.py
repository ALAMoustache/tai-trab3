import sys
from subprocess import check_output

def nccdScore(tar, ref1, ref2, ref3, ctx):
	ImgCondComp = "../bin/ImgCondComp"
	quantizationFactor = str(32)
	x = tar
	y1, y2, y3 = ref1, ref2, ref3

	Cx = check_output([ImgCondComp, "-q", quantizationFactor, "-tc", ctx, "-t", x])
	Cy = check_output([ImgCondComp, "-q", quantizationFactor, "-tc", ctx, "-t", y1, "-t", y2, "-t", y3])
	Cy = compressionBits(Cy)
	Cylx = check_output([ImgCondComp, "-q", quantizationFactor, "-rc", ctx, "-tc", ctx, "-r", x, "-t", y1, "-t", y2, "-t", y3])
	Cylx = compressionBits(Cylx)

	return float("%.4f" % (Cylx/Cy))

def compressionBits(comp):
	start = ": "
	end = " ("
	return comp[comp.find(start)+len(start):comp.find(end)]
