from NCD import ncdScore
import shutil, os
from sys import argv
import time

dirName = 'tempDir'

nPersons = 40
nImagesPerson = 10

#print(argv[0])
compressAlgorithm = ""
if len(argv) < 2 or len(argv) > 4:
	print("Insert compression algorithm [lzma, bzip2, zip_deflated, gzip]")
	print("Optional parameters\n\t[negative] -> Inverts color scheme of souce images\n\t[resize] -> Resizes images to 55 x 46")
	print("Examples:\n\t[1] python3 -B TEST_NCD.py lzma\n\t[2] python3 -B TEST_NCD.py lzma negative")
	exit()
if len(argv) >= 2 and (argv[1] == "lzma" or argv[1] == "bzip2" or argv[1] == "zip_deflated" or argv[1] == "gzip"):
	compressAlgorithm = str(argv[1])
else:
	print("Insert one of the following compression algorithms [lzma, bzip2, zip_deflated, gzip]\tTyped: ", argv[1])
	exit()
print("Compression Method: ", compressAlgorithm)

negative = False
resize = False
if len(argv) >= 3:
	#for arg in argv[2:-1]:
	for arg in range(2, len(argv)):
		#print("ARGV: ", arg)
		if argv[arg] == "negative":
			negative = True
		elif argv[arg] == "resize":
			resize = True
		else:
			print("Negative Status: ", negative, "\nResize Status: ", resize, "\tTyped: ", argv[arg])

print("Negative Status: ", negative)
print("Resize Status: ", resize)

'''if len(argv) == 3 and argv[2] == "negative":
	negative = True
	print("Negative Status: ", negative)
elif len(argv) == 3:
	negative = False
	print("Negative Status: ", negative, "\tTyped: ", argv[2])
elif len(argv) < 3:
	negative = False
	print("Negative Status: ", negative)
'''
def main():
	totalErrors = 0
	for s in range(1, nPersons+1):
		tarSubject = s
		print("%02d: " % (tarSubject), end="")
		#print("%02d: " % (tarSubject)), # Python 2.7
		minSubject = None
		errors = 0
		for version in range(4, nImagesPerson+1):
			tar = "../orl_faces/s%02d/%02d.pgm" % (tarSubject, version)
			#print("tar", tar, end="")
			minScore = 1000
	#commen next line
			#print("picture ", version)
			#print("version ", version),
			for r in range(1, nPersons+1):
				refSubject = r
	#uncomment next line?
	#			print("%02d " % (refSubject)),
				ref1 = "../orl_faces/s%02d/01.pgm" % (refSubject)
				ref2 = "../orl_faces/s%02d/02.pgm" % (refSubject)
				ref3 = "../orl_faces/s%02d/03.pgm" % (refSubject)
				score = ncdScore(tar, ref1, ref2, ref3, dirName, compressAlgorithm, negative, resize)
				#print("Score: ", score, score<minScore)
				if score < minScore:
					minScore = score
					minSubject = refSubject
			print ("%02d " % (minSubject), end="")
			#print ("%02d " % minSubject), # Python 2.7
			if minSubject != tarSubject:
				errors+=1
		totalErrors += errors
		print("-> ", errors)
	print("Total errors: ", totalErrors)

# Create directory
def createDirectory():
	try:
		# Create target Directory
		os.mkdir(dirName)
		print("Directory " , dirName ,  " Created ") 
	except FileExistsError:
		print("Directory " , dirName ,  " already exists")

def deleteDirectory():
	# Remove cache & temp folder
	# python3 -B [file] -> does not generate cache
	#shutil.rmtree('__pycache__')
	try:
		# Delete target Directory
		shutil.rmtree(dirName)
		print("Directory " , dirName ,  " Deleted ") 
	except FileExistsError:
		print("Directory " , dirName ,  " was not deleted")


createDirectory()

start_time = time.time()
main()
print("--- %s seconds ---" % (time.time() - start_time))

deleteDirectory()