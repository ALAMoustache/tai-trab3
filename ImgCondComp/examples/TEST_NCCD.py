from NCCD3 import nccdScore
from sys import argv

nPersons = 40
nImagesPerson = 10

if len(argv) == 2:
    if argv[1] == "ctx1" or argv[1] == "ctx2":
        context = argv[1]
    else:
        print("Select one of the following contexts\n\t[1] ctx1\n\t[2] ctx2")
        exit()
else:
    print("Examples:\n\t[1] python3 -B TEST_NCCD.py ctx1\n\t[2] python3 -B TEST_NCCD.py ctx2")


def main():
	totalErrors = 0
	for s in range(1, nPersons+1):
		tarSubject = s
		print("%02d: " % (tarSubject), end="")
		minSubject = None
		errors = 0
		for version in range(4, nImagesPerson+1):
			tar = "../orl_faces/s%02d/%02d.pgm" % (tarSubject, version)
			minScore = 1000
			for r in range(1, nPersons+1):
				refSubject = r
				ref1 = "../orl_faces/s%02d/01.pgm" % (refSubject)
				ref2 = "../orl_faces/s%02d/02.pgm" % (refSubject)
				ref3 = "../orl_faces/s%02d/03.pgm" % (refSubject)
				score = nccdScore(tar, ref1, ref2, ref3, context)
				if score < minScore:
					minScore = score
					minSubject = refSubject
			print ("%02d " % (minSubject), end="")
			if minSubject != tarSubject:
				errors+=1
		totalErrors += errors
		print("-> ", errors)
	print("Total errors: ", totalErrors)

main()