from PIL import Image
import numpy as np
import zipfile as zf
import gzip
from os.path import getsize
import gzip

'''
'' x = target image
'' y1 = reference image 1
'' y2 = reference image 2
'' y3 = reference image 3
'''
def ncdScore(tar, ref1, ref2, ref3, dirName, compressAlgorithm, negative, resize):
	if compressAlgorithm == "lzma":
		compressAlgorithm=zf.ZIP_LZMA # Tipo de compressão
	elif compressAlgorithm == "bzip2":
		compressAlgorithm=zf.ZIP_BZIP2 # Tipo de compressão
	elif compressAlgorithm == "zip_deflated":
		compressAlgorithm=zf.ZIP_DEFLATED # Tipo de compressão

	if negative is False:
		colorValue = 1
	else:
		colorValue = 255

	#print("tar: ", tar, "\nref1:", ref1, "\nref2:", ref2, "\nref3:", ref3)
	
	# Cx
	imgx = Image.open(tar)
	if resize == True:
		imgx = imgx.resize((55,46),Image.ANTIALIAS)
	imageArrayX = np.array(imgx)
	resultImageX = Image.fromarray((imageArrayX * colorValue).astype(np.uint8))
	resultImageX.save('./' + dirName + '/outputImageX.pgm')

	if compressAlgorithm == "gzip":
		outputFileName = './' + dirName + '/target.zip'
		zipfile = gzip.open(outputFileName, 'wb', compresslevel=9)

		imgByteArray = resultImageX.tobytes()
		zipfile.write(imgByteArray)
		zipfile.close()

	else:
		zipfile = zf.ZipFile('./' + dirName + '/target.zip', 'w')
		zipfile.write('./' + dirName + '/outputImageX.pgm', 'outputImageX.pgm', compress_type=compressAlgorithm)	# Compressão ZIP

	targetCompressedSize = getsize('./' + dirName + '/target.zip')

	# Cy
	imgConcatenation = None
	#zipfile = zf.ZipFile('./' + dirName + '/referencesConcatenated.zip', 'w')
	references = [ref1, ref2, ref3]
	for refImage in references:
		#refImage = "ref" + str(ref)
		imgy=Image.open(refImage)
		if resize == True:
			imgy = imgy.resize((55,46),Image.ANTIALIAS)
		imageArray = np.array(imgy)
		if imgConcatenation is None:
			imgConcatenation = imageArray.T
		else:
			imgConcatenation = np.concatenate((imgConcatenation, imageArray.T))
	
	result = Image.fromarray((imgConcatenation.T * colorValue).astype(np.uint8))
	result.save('./' + dirName + '/referencesConcatenated.pgm')

	if compressAlgorithm == "gzip":
		outputFileName = './' + dirName + '/referencesConcatenated.zip'
		zipfile = gzip.open(outputFileName, 'wb', compresslevel=9)

		imgByteArray = result.tobytes()
		zipfile.write(imgByteArray)
		zipfile.close()

	else:
		zipfile = zf.ZipFile('./' + dirName + '/referencesConcatenated.zip', 'w')
		zipfile.write('./' + dirName + '/referencesConcatenated.pgm', 'referencesConcatenated.pgm', compress_type=compressAlgorithm)	# Compressão ZIP

	referencesCompressedSize = getsize('./' + dirName + '/referencesConcatenated.zip')

	# Cxy
	imgConcatenation = None
	#zipfile = zf.ZipFile('./allCompressed.zip', 'w')
	allImages = [tar, ref1, ref2, ref3]
	for refImage in allImages:
		imgxy=Image.open(refImage)
		if resize == True:
			imgxy = imgxy.resize((55,46),Image.ANTIALIAS)
		imageArray = np.array(imgxy)
		if imgConcatenation is None:
			imgConcatenation = imageArray.T
		else:
			imgConcatenation = np.concatenate((imgConcatenation, imageArray.T))
	
	result = Image.fromarray((imgConcatenation.T * colorValue).astype(np.uint8))
	result.save('./' + dirName + '/allCompressed.pgm')

	if compressAlgorithm == "gzip":
		outputFileName = './' + dirName + '/allCompressed.zip'
		zipfile = gzip.open(outputFileName, 'wb', compresslevel=9)

		imgByteArray = result.tobytes()
		zipfile.write(imgByteArray)
		zipfile.close()

	else:
		zipfile = zf.ZipFile('./' + dirName + '/allCompressed.zip', 'w')
		zipfile.write('./' + dirName + '/allCompressed.pgm', 'allCompressed.pgm', compress_type=compressAlgorithm)	# Compressão ZIP

	allCompressedSize = getsize('./' + dirName + '/allCompressed.zip')

	ncd = (allCompressedSize - min(targetCompressedSize, referencesCompressedSize)) / max(targetCompressedSize, referencesCompressedSize)
	return ncd

	

